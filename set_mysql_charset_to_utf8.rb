ActiveRecord::Base.connection.tables.select do |t|
  ActiveRecord::Base.connection.execute("show create table #{t}").to_a[0][1].match(/CHARSET=latin1/)
end.each do |t|
  ActiveRecord::Base.connection.execute("alter table #{t} CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;")
end